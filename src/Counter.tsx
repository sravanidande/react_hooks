import * as React from 'react'
import { useState } from 'react'
import './style.css'

interface CounterProps {
  readonly count: number
  readonly onIncrement: () => void
  readonly onDecrement: () => void
}

export const Counter: React.SFC<CounterProps> = ({
  count,
  onIncrement,
  onDecrement,
}) => (
  <section className="section">
    <div>
      <button className="button is-primary" onClick={onIncrement}>
        +
      </button>
      <h1 className="title is-1">{count}</h1>
      <button className="button is-danger" onClick={onDecrement}>
        -
      </button>
    </div>
  </section>
)

type counter = { readonly count: number }

export const CounterView = () => {
  useState<counter>({ count: 0 })
  const [state, setState] = useState(0)
  const onIncrement = () => setState(state + 1)
  const onDecrement = () => setState(state - 1)

  // you cannot always pass 0, otherwize it will always remain 0. you should pass state, that's what holds your increment count value
  // got it?

  return (
    <Counter
      count={state}
      onIncrement={onIncrement}
      onDecrement={onDecrement}
    />
  )
}
