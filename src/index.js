import * as React from 'react'
import { render } from 'react-dom'
import { CounterView } from './Counter'

const App = () => (
  <div>
    <CounterView />
  </div>
)

render(<App />, document.getElementById('root'))
